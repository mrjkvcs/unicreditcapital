<?php

/**
 * Welcome
 */
get('/', 'WelcomeController@index');

/**
 * Registration
 */
get('register', [
    'as'   => 'register_path',
    'uses' => 'RegistrationController@create'
]);

post('register', [
    'as'   => 'register_path',
    'uses' => 'RegistrationController@store'
]);

/**
 * Sessions
 */
get('login', [
    'as'   => 'login_path',
    'uses' => 'SessionsController@create'
]);

post('login', [
    'as'   => 'login_path',
    'uses' => 'SessionsController@store'
]);

get('logout', [
    'as'   => 'logout_path',
    'uses' => 'SessionsController@destroy'
]);

/**
 * Pages
 */
get('home', [
    'as'   => 'home',
    'uses' => 'PagesController@home'
]);

get('about', [
    'as'   => 'about_path',
    'uses' => 'PagesController@about'
]);

get('solutions', [
    'as'   => 'solutions_path',
    'uses' => 'PagesController@solutions'
]);

/**
 * Contact
 */
get('contact', [
    'as'   => 'contacts_path',
    'uses' => 'ContactsController@create'
]);

post('contact', [
    'as'   => 'contacts_path',
    'uses' => 'ContactsController@store'
]);

/**
 * Products pages
 */
get('real-estate', [
    'as'   => 'real_estate_path',
    'uses' => 'ProductsController@realEstate'
]);

get('private-lending', [
    'as'   => 'private_lending',
    'uses' => 'ProductsController@privateLending'
]);

get('tax-liens', [
    'as'   => 'tax_liens',
    'uses' => 'ProductsController@taxLiens'
]);

get('private-placements', [
    'as'   => 'private_placements',
    'uses' => 'ProductsController@privatePlacements'
]);

get('eb-5-visa', [
    'as'   => 'eb_5_visa',
    'uses' => 'ProductsController@eb5Visa'
]);

get('other-alternative-investments-allowed-in-your-ira', [
    'as'   => 'alternative_investments',
    'uses' => 'ProductsController@alternativeInvestments'
]);

//Route::controllers([
//	'auth' => 'Auth\AuthController',
//	'password' => 'Auth\PasswordController',
//]);
