<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

    /**
     * Home page
     *
     * @return \Illuminate\View\View
     */
    public function home()
    {
        $style = 'index';
        $navbar = 'navbar-fixed-top';
        $body = 'class="pull_top"';
        $imgWidth = '200px';

        return view('pages.home', compact('body', 'style', 'navbar', 'imgWidth'));
    }

    /**
     * About page
     *
     * @return \Illuminate\View\View
     */
    public function about()
    {
        $style = 'about';

        return view('pages.about', compact('style'));
    }

    /**
     * Solution page
     *
     * @return \Illuminate\View\View
     */
    public function solutions()
    {
        $style = 'about';

        return view('pages.solutions', compact('style'));
    }

}
