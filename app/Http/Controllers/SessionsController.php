<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SigninUserRequest;
use Auth;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class SessionsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $style = 'sign-in';

        return view('sessions.create', compact('style'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SigninUserRequest $request
     * @return Response
     */
    public function store(SigninUserRequest $request)
    {
        $formData = $request->only(['email', 'password']);

        if ( ! Auth::attempt($formData))
        {
            Flash::error('We were unable sign you in. Please check your credentials and try again!');

            return redirect()->back()->withInput($formData);
        }

        return redirect()->intended('home');
    }

    /**
     * User logout
     *
     * @return Response
     * @internal param int $id
     */
    public function destroy()
    {
        Auth::logout();

        return redirect('home');
    }
}
