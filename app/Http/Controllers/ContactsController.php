<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;

class ContactsController extends Controller {

	public function create()
    {
        $style = 'contact';
        $footer = 'style="margin-top:0;"';

        return view('contacts.create', compact('style', 'footer'));
    }

    public function store(CreateContactRequest $request)
    {
        $data = $request->only(['name', 'email', 'phone', 'messages']);

        Mail::send('contacts.contact', $data, function($message)
        {
            $message->to('csabameiszburger@me.com')->subject('Customer');
        });

        Flash::success('Your message has been sent.');

        return redirect('home');
    }

}
