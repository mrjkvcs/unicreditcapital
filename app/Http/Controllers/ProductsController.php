<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ProductsController extends Controller {

    /**
     * Real Estate page
     *
     * @return mixed
     */
    public function realEstate()
    {
        $style = 'about';

        return view('products.real-estate', compact('style'));
    }

    /**
     * Private Lending page
     *
     * @return mixed
     */
    public function privateLending()
    {
        $style = 'about';

        return view('products.private-lending', compact('style'));
    }

    /**
     * Tax Liens page
     *
     * @return mixed
     */
    public function taxLiens()
    {
        $style = 'about';

        return view('products.tax-liens', compact('style'));
    }

    /**
     * Private Placements page
     *
     * @return mixed
     */
    public function privatePlacements()
    {
        $style = 'about';

        return view('products.private-placements', compact('style'));
    }

    /**
     * EB5 Visa page
     *
     * @return mixed
     */
    public function eb5Visa()
    {
        $style = 'about';

        return view('products.eb-5-visa', compact('style'));
    }

    /**
     * Alternative Investments page
     *
     * @return mixed
     */
    public function alternativeInvestments()
    {
        $style = 'about';

        return view('products.alternative-investments', compact('style'));
    }
}
