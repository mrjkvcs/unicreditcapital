<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;

class RegistrationController extends Controller {

    /**
     * Register a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $style = 'sign-up';

        return view('registration.create', compact('style'));
    }

    /**
     * Save user
     *
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateUserRequest $request)
    {
        $user = User::create($request->all());

        Auth::login($user);

        Mail::send('emails.welcome', ['name' => $user->name], function($message) use ($user)
        {
            $message->to($user->email)->subject('Welcome to UniCreditCapital');
        });

        Flash::success('You have been registered successfully.');

        return redirect('home');
    }

}
