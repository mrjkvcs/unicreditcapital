@extends('app')

@section('content')

    @include('partials.sliders')

    <div id="showcase">
        <div class="container">
            <div class="section_header">

                @include('errors.lists')
                @include('flash::message')

                <h3>Our Services</h3>
            </div>
            <div class="row feature_wrapper">
                <!-- Features Row -->
                <div class="features_op1_row">
                    <!-- Feature -->
                    <div class="col-sm-4 feature first">
                        <div class="img_box">
                            <a href="{!! route('real_estate_path') !!}">
                                <img src="/images/service1.png">
                                    <span class="circle">
                                        <span class="plus">&#43;</span>
                                    </span>
                            </a>
                        </div>
                        <div class="text">
                            <h6>Retirement</h6>
                            <p>
                                UniCredit Capital delivers the best solution for languishing retirement accounts and underperforming assets by rolling over or transferring those funds into a self-directed IRA where your purchase is held while substantial tax-deferred profit accrues.
                            </p>
                        </div>
                    </div>
                    <!-- Feature -->
                    <div class="col-sm-4 feature">
                        <div class="img_box">
                            <a href="{!! route('real_estate_path') !!}">
                                <img src="/images/service2.png">
                                    <span class="circle">
                                        <span class="plus">&#43;</span>
                                    </span>
                            </a>
                        </div>
                        <div class="text">
                            <h6>Home & Family</h6>
                            <p>
                                UniCredit Capital offers an excellent source of future funds for the ongoing expense of owning, renting, or leasing and maintaining a home… and providing for you and your family in the manner you desire.
                            </p>
                        </div>
                    </div>
                    <!-- Feature -->
                    <div class="col-sm-4 feature last">
                        <div class="img_box">
                            <a href="{!! route('real_estate_path') !!}">
                                <img src="/images/service4.png">
                                    <span class="circle">
                                        <span class="plus">&#43;</span>
                                    </span>
                            </a>
                        </div>
                        <div class="text">
                            <h6>Wealth Protection </h6>
                            <p>
                                UniCredit  Capital has a proven strategy toward not only building significant wealth, but also protecting that wealth from loss. In an age when corporate bankruptcies are increasing and stock holdings collapse, UniCredit Capital provides a safe haven for your valuable finances.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="features">
        <div class="container">
            <div class="section_header">
                <h3>Features</h3>
            </div>
            <div class="row feature">
                <div class="col-sm-12 info">
                    <h3>
                        Real Estate IRAs
                    </h3>
                    <p> Historically, real estate has given many Americans with a stable investment vehicle that offers both income and appreciation. One of the greatest tools available to real estate investors is the self-directed IRA – a government-sponsored retirement plan that allows real property investments. </p>
                    <p>Most investors believe that their only IRA investment options are bank CDs, the stock market, and mutual funds.</p>
                    <p>Few Americans realize that they have the option to self-direct their IRAs and other retirement plans into real estate—and that they can benefit from the tax advantages those plans provide.</p>
                    <p>IRA investments earn tax-deferred/tax-free profits. Imagine not having to pay taxes right away—or ever—on your real estate deals. Instead of paying 25%, or 30%, or even 50% of your profits to the government in taxes, you keep it.</p>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    @include('partials.footer-main-page')
@endsection
