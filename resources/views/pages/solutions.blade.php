@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Solutions</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        Please take a moment to peruse the list of key areas we’ve isolated for which we believe UniCredit Capital has the perfect solution. Review any one of  the areas listed to learn how UniCredit Capital Premium Investment strategie provides a powerful, safe, and secure solution to all!
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/solutions/solution-1.jpg" alt=""/>
                            </li>
                            <li>
                                <img src="images/solutions/solution-2.jpg" alt=""/>
                            </li>
                            <li>
                                <img src="images/solutions/solution-3.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 intro">
                    <h6>Retirement</h6>
                    <p>
                        One of the principal components of most financial plans is preparation for a comfortable retirement. We all work hard during our prime years so that in our golden years we can have a sense of safety, security, and stability while we enjoy some of the finer things in life. UniCredit Capital Premium Investment provides the means to ensure a very comfortable retirement for every individual and family.
                    </p>
                    <h6>Home & Family</h6>
                    <p>
                        The needs of home and family are as diverse as they are critically important to each of us. When the expenses surrounding maintaining a home and providing for a family are at all-time highs, UniCredit Capital Premium Investment provides an extremely effective tool toward ensuring that you will have the necessary resources to address those needs in the manner to which you’re accustomed… and beyond.
                    </p>
                    <h6>Wealth Protection</h6>
                    <p>
                        Surrounding all considerations in building wealth and managing our financial resources effectively is the key element of protection. Far too many contemporary investment vehicles (stock market; mutual and money market funds; traditional savings; etc.) either bring very low returns or they are fraught with crisis, volatility, and unnecessary risk. UniCredit Capital Premium Investment provides the time-tested power, safety, and security of Real Estate ownership as one of the finest means to build and protect your financial future.
                    </p>
                    <p>
                        UniCredit Capital Premium Investment is the time-tested strategy of purchasing carefully researched and selected properties in the direct path of population growth and development surrounding a major metropolitan area and then holding that property until its value matures. Once profit accrues in the property, the investor(s) sells the house(s) to new owners for a significant return on their investment. We refer to this strategy as our UniCredit Capital Premium Investment “solutions” to the financial challenges facing most people today. Whether your goal is a comfortable retirement; providing for your children’s or grandchildren’s college education; preserving your house and home in the manner which you desire; ensuring adequate funds beyond insurance for emergency and/or long-term medical care; supplying extra resources for travel and leisure activities; establishing an enduring legacy; or simply building, protecting, and enhancing your personal wealth, UniCredit Capital Premium Investment is the solution!
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
