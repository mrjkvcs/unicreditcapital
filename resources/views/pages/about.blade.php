@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>About Us</h3>
            </div>
            <div class="row">
                <div class="col-sm-12 intro">
                    <p>
                        UniCredit Capital is a company with headquarters based in Las Vegas, Nevada for over 10 years. Our current business evolved from the extremely successful Real Estate Investment experience we enjoyed at the time of the Financial Downturn.
                    </p>
                    <p>
                        UniCredit Capital has been a pioneer in the field of Private Capital, REO, and the Foreclosure Market for over 10 years. Our proven approach has made it possible for UniCredit Capital customers to allocate their underperforming assets to more attractive investments. With the leverage of UniCredit Capital’s experience and expertise, our customers have access to asset portfolios with better pricing and exit strategies than any other secure investment product available today.
                    </p>
                    <p>
                        UniCredit Capital is neither a real estate broker nor a financial advisor.  We are principals who research, acquire, and distribute carefully selected Foreclosed houses and REO's specifically for the purpose of obtaining secure Investments. The goal of UniCredit Capital is to assist our customers in capitalizing on the power, safety, and security, of the entire investment process.  Our purpose is to educate and enlighten as many people as possible regarding the potential for extraordinary profits. Our successful business model is based on the keen insight expressed by John D. Rockefeller when he stated, “The major fortunes in America have been made in Real Estate Investments.”
                    </p>
                    <p>
                        Whether you’re an astute wealth builder preparing for your retirement, saving for your children’s education, or establishing a lasting legacy for your family, we are confident you’ll find that UniCredit Capital has a time-tested and proven formula that provides one the absolute best solutions for financial fitness and security.
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
