@extends('app')

@section('content')

    <div id="sign_in2">
        <div class="container">
            <div class="section_header">

                @include('errors.lists')

                <h3>Sign in</h3>
            </div>
            <div class="row login">
                <div class="col-sm-5 left_box">
                    <h4>Log in to your account</h4>

                    <p><strong>UniCredit Capital Self-Directed IRA Success Kit Reveals:</strong></p>

                    <div class="perk_box">
                        <div class="perk">
                            <p>The #1 Wealth Creating Tip</p>
                        </div>
                        <div class="perk">
                            <p>How to Earn Tax-Free Profits Investing in Positive CashFlow LLCs</p>
                        </div>
                        <div class="perk">
                            <p>How Easy it is to Get Started on the Path to Financial Freedom</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 signin_box">
                    <div class="box">
                        <div class="box_cont">

                            @include('partials.social-icons')

                            <div class="division">
                                <div class="line l"></div>
                                <span>or</span>
                                <div class="line r"></div>
                            </div>

                            <div class="form">

                                {!! Form::open(['url' => '/auth/login']) !!}
                                    <input type="text" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">
                                    <input type="text" placeholder="Password" name="password" class="form-control">
                                    <div class="forgot">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                    <input type="submit" value="sign in">
                                {!! Form::close() !!}

                                <div class="forgot">
                                    <br/>
                                    <a href="/password/email">Forgot Your Password?</a>
                                </div>
                                <div class="forgot">
                                    <span>Don’t have an account?</span>
                                    {!! link_to('auth/register', 'Sign up') !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    @include('partials.footer')
@endsection
