@extends('app')

@section('content')
    <div id="sign_up2">
        <div class="container">
            <div class="section_header">

                @include('errors.lists')

                <h3>Sign up </h3>
            </div>
            <div class="row login">
                <div class="col-sm-5 left_box">
                    <h4>Create your account</h4>

                    <div class="perk_box">
                        <div class="perk">
                            <p><strong>Take Control of Your Financial Future</strong></p>
                            <p>Discover what only 2% of Americans know about creating lasting wealth. Truly Self-Directed IRAs and other retirement plans allow you to create lasting wealth by investing in areas where you have knowledge, expertise, and comfort.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 signin_box">
                    <div class="box">
                        <div class="box_cont">

                            @include('partials.social-icons')

                            <div class="division">
                                <div class="line l"></div>
                                <span>or</span>
                                <div class="line r"></div>
                            </div>

                            <div class="form">

                                {!! Form::open(['url' => '/auth/register']) !!}
                                    <input type="text" placeholder="Name" class="form-control" name="name" value="{{ old('name') }}">
                                    <input type="text" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}">
                                    <input type="text" placeholder="Password" name="password" class="form-control">
                                    <input type="text" placeholder="Confirm Password" class="form-control" name="password_confirmation">
                                    <div class="forgot">
                                        <span>Already have an account?</span>
                                        {!! link_to('auth/login', 'Sign in') !!}
                                    </div>
                                    <input type="submit" value="sign up">
                                {!! Form::close() !!}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    @include('partials.footer')
@endsection
