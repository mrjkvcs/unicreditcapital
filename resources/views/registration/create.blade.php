@extends('app')

@section('content')
    <div id="sign_up2">
        <div class="container">
            <div class="section_header">

                @include('errors.lists')

                <h3>Sign up </h3>
            </div>
            <div class="row login">
                <div class="col-sm-5 left_box">
                    <h4>Create your account</h4>

                    <div class="perk_box">
                        <div class="perk">
                            <p><strong>Take Control of Your Financial Future</strong></p>
                            <p>Discover what only 2% of Americans know about creating lasting wealth. Truly Self-Directed IRAs and other retirement plans allow you to create lasting wealth by investing in areas where you have knowledge, expertise, and comfort.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 signin_box">
                    <div class="box">
                        <div class="box_cont">

                            {{--@include('partials.social-icons')--}}

                            {{--<div class="division">--}}
                                {{--<div class="line l"></div>--}}
                                {{--<span>or</span>--}}
                                {{--<div class="line r"></div>--}}
                            {{--</div>--}}

                            <div class="form">

                                {!! Form::open(['route' => 'register_path']) !!}

                                    <!-- Name Form Input -->
                                    <div class="form-group">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                                    </div>

                                    <!-- Email Form Input -->
                                    <div class="form-group">
                                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                                    </div>

                                    <!-- Password Form Input -->
                                    <div class="form-group">
                                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                                    </div>

                                    <!-- Password Confirmation Form Input -->
                                    <div class="form-group">
                                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password Confirmation']) !!}
                                    </div>

                                    <div class="forgot">
                                        <span>Already have an account?</span>
                                        {!! link_to_route('login_path', 'Sign in') !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::submit('Sign up', ['class' => 'btn btn-primary']) !!}
                                    </div>

                                {!! Form::close() !!}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    @include('partials.footer')
@endsection
