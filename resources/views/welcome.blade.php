<html>
	<head>
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>

		<style>
			body {
                background-color: #111;
            }

            img {
                width: 75%;
            }

            .row h3 {
                margin-top: 70px;
                color: #C58227;
                font-weight: 100;
            }

            .yellow-button {
                margin-top: 50px;
                background-color: #C58227;
                border-color: #D09135;
                padding: 20px 40px;
            }

            .yellow-button:hover {
                background-color: #DCA428;
            }
		</style>
	</head>
	<body>
		<div class="container text-center" style="margin-top: 20px">
            <div class="row">
                <div class="col-md-12">
                    <img src="/images/logo.jpg" alt=""/>
                </div>
            </div>
            <div class="row"><h3>DISCOVER THE POWER OF SELF - DIRECTED IRAs</h3></div>
            <div class="row">
                <div class="col-md-12">
                    <a href="/home" class="btn btn-primary btn-lg yellow-button ">ENTER</a>
                </div>
            </div>
		</div>
	</body>
</html>
