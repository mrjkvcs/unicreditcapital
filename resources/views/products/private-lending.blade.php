@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Private Lending with a Self-Directed IRA</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        Many UniCredit Capital clients use their self-directed IRAs to lend money to others for real estate projects, private businesses or other investments. Lending money is often a way that investors can passively invest with their self-directed IRA, possibly generating a positive return with the security of knowing that the investment is most likely collateralized.
                    </p>
                    <p>
                        When planning to originate loans, it is important that before lending money to any person or entity that due diligence is conducted on the investment. UniCredit Capital will research applicable lending laws and have clear goals of what they hope to accomplish, short term profit, stability, 12% growth, etc. just to name a few. Many times, people that are borrowing money from you are coming to you because they have had problems with credit history or overextended credit. Background checks, collateral, and possible co-signers are options that will give the person lending money piece of mind.
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/products/private-lending-1.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
