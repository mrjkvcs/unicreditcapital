@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Obtain EB-5 Visas ( Immigrant Investor )</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        The EB-5 visa provides a method of obtaining a green card for foreign nationals who invest money in the United States. To obtain the visa, individuals must invest $1,000,000 (or at least $500,000 in a "Targeted Employment Area" - high unemployment or rural area), creating or preserving at least 10 jobs for U.S. workers excluding the investor and their immediate family. Initially, under the first EB-5 program, the foreign investor was required to create an entirely new commercial enterprise; however, under the Pilot Program investments can be made directly in a job-generating commercial enterprise (new, or existing - "Troubled Business"), or into a "Regional Center" - a 3rd party-managed investment vehicle (private or public), which assumes the responsibility of creating the requisite jobs. Regional Centers may charge an administration fee for managing the investor's investment.
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/products/eb5-visa-1.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 intro">
                    <p>
                        If the foreign national investor's petition is approved, the investor and their dependents will be granted conditional permanent residence valid for two years. Within the 90 day period before the conditional permanent residence expires, the investor must submit evidence documenting that the full required investment has been made and that 10 jobs have been maintained, or 10 jobs have been created or will be created within a reasonable time period.
                    </p>
                    <p>
                        In 1992, Congress created a temporary pilot program designed to stimulate economic activity and job growth, while allowing eligible aliens the opportunity to become lawful permanent residents. Under this pilot program, foreign nationals may invest in a pre-approved regional center, or "economic unit, public or private, which is involved with the promotion of economic growth, including increased export sales, improved regional productivity, job creation, or increased domestic capital investment". Investments within a regional center provide foreign nationals the added benefit of allowing them to count jobs created both directly and indirectly for purposes of meeting 10-job creation requirement.
                    </p>
                    <h6>Targeted Employment Area</h6>
                    <p>
                        USCIS defines a targeted employment area (TEA) as an area which, at the time of investment, is a rural area (not within either a metropolitan statistical area (MSA) (as designated by the Office of Management and Budget) or the outer boundary of any city or town having a population of 20,000 or more), OR an area within an MSA or the outer boundary of a city or town having a population of 20,000 or more which has experienced unemployment of at least 150% of the national average rate.
                    </p>
                    <p>
                        If the location of the proposed new business is not a TEA, the investor has the option to gather the relevant publicly available state or federal statistics on their own and submit it with their petition for USCIS to have a new TEA determination made. In California, the investor may petition the state government for designating a particular subdivision of the area as an area of high unemployment (over 150% the national average); however, this designation is not made by USCIS.
                    </p>
                    <p>
                        There is no centralized list of targeted employment areas. State agencies in California, Florida and Washington maintain lists of TEAs.
                    </p>
                    <h6>Pre-Immigration Tax Planning</h6>
                    <p>
                        Tax planning prior to immigration prepares the prospective immigrants to structure their assets by taking in consideration the differences in the U.S. tax system. An important point is that United States has a worldwide taxation system, i.e. all income earned by U.S. persons (citizens and residents) in the U.S. and outside the U.S. is subject to taxation by the U.S. government. Once a foreigner becomes a permanent resident, and therefore a U.S. taxpayer, any income that person generates anywhere else in the world will be taxed by the U.S. In addition, when the immigrant dies, all their assets, anywhere in the world will be subject to a 40% estate tax by the U.S. Pre-immigration tax planning helps in understanding and structuring an immigrant’s assets and investments to their best tax advantage, such as removing capital gain prior to immigration, structuring ownership of passive assets to avoid U.S. income taxation following immigration, and structuring ownership of all assets to avoid U.S. estate taxation on death.
                    </p>
                    <h6> EB-5 Regional Center projects </h6>
                    <p>
                        USCIS maintains a list of approved (which does not signify endorsement) EB-5 (Immigrant Investor) Regional Centers by state, but without details. EB-5 Funding has helped rebuild the Las Vegas economy. Las Vegas has seen a recent boom in using regional centers and EB-5 funding to build new casino projects. In 2013 the first EB-5 project the Downtown Grand was built followed by SLS formerly the Sahara Hotel. Additional New Casino Projects obtaining EB-5 funding on the Las Vegas strip include Lucky Dragon, Dynasty Hotel Casino, Clarion Hotel, and World Resorts.
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
