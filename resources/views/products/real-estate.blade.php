@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Real Estate</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        A real estate IRA is technically no different than any other IRA (or 401k). The government created the IRA to allow investments to grow tax-free or tax-deferred compounded over time to maximize growth. The IRA can also qualify for yearly tax-deductions (depending on the account), provide asset protection and assets can be passed to future generations.
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/products/real-estate-1.jpg" alt=""/>
                            </li>
                            <li>
                                <img src="images/products/real-estate-2.jpg" alt=""/>
                            </li>
                            <li>
                                <img src="images/products/real-estate-3.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row m-top-50">
                <div class="col-sm-12 intro">
                    <p>
                        A real estate IRA is unique because it allows investments in real property.
                    </p>
                    <p>
                        Historically, real estate has been a stable investment vehicle for many Americans that can provide both income and appreciation. Various ways to invest in real estate exist, from a more passive strategy, including purchasing property or land that grows in appreciation over time, to more active strategies, including acquiring property, rehabbing it and managing it as a rental or attempting to quickly re-sell the property for profit. In addition, there are many investment strategies surrounding the contracts, options and mortgages tied to real property.
                    </p>
                    <p>
                        Real estate and similar assets are among the most popular options for UniCredit Capital clients. Here's a partial list of real estate-related investments that you can make when you self-direct your IRA at UniCredit Capital :
                    </p>
                    <ul>
                        <li>Residential</li>
                        <li>Commercial</li>
                        <li>Raw land</li>
                        <li>Mobile homes</li>
                        <li>Foreign real estate</li>
                    </ul>
                    <p>
                        In addition, there are a number of acquisition and selling strategies involved in real estate investing, including:
                    </p>
                    <ul>
                        <li>Rehabbing and Reselling</li>
                        <li>Foreclosures and REO’s (real estate owned)</li>
                        <li>Wholesaling</li>
                        <li>Options</li>
                    </ul>
                    <p>
                        Real estate can be an important part of a balanced investment portfolio, providing a tangible asset that can provide ongoing income and appreciate in value. As with any investment there are risks, and inexperienced real estate investors should consider the ongoing costs to buy, manage and sell real estate, the time requirements to manage, the complexities of real estate contracts, titles, insurance, etc., and the cyclical nature of the market.
                    </p>
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
