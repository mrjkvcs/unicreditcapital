@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Invest in Private Placements with Self-Directed IRA</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        UniCredit Capital provides all neccessary experties to safely invest in a self-directed IRA  in private placements or private-held entities, possibly growing their account by being involved with such arrangements as Limited Partnerships, LLCs, C-Corporations and Land/Personal Property Trusts. The IRS does not allow investing in General Partnerships or subchapter S Corporations.
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/products/private-placements-1.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row m-top-50">
                <div class="col-sm-12 intro">
                    <h6>Important things to consider when investing in private placements with a self-directed IRA:</h6>
                    <ul>
                        <li>If it is an existing entity – total ownership of disqualified individuals (including the IRA owner) cannot be 50% or higher. If it is, the IRA cannot invest in it.</li>
                        <li>If the IRA is running a business in a flow through entity (i.e. Limited Partnership or LLC) it may be subject to UBIT (Unrelated Business Income Tax).</li>
                        <li>The IRA owner cannot work for a business owned by their IRA</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
