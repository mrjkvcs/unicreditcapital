@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Other Alternative Investments Allowed in Your IRA</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        Beyond many of the popular alternative investments available for self-directed IRAs (real estate, tax liens, private lending, notes, and private placements), there are even more possibilities available for self-directed investors. As long as IRS guidelines are followed, you are in direct control of your financial future and UniCredit Capital can determine what investment makes the most sense for you.
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/products/alternative-statements-1.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row m-top-50">
                <div class="col-sm-12 intro">
                    <h6>Additional self-directed IRA investment possibilities:</h6>
                    <ul>
                        <li>Accounts Receivable Financing</li>
                        <li>Building Bonds</li>
                        <li>Commercial Paper</li>
                        <li>Equipment Leasing</li>
                        <li>Factoring Investments</li>
                        <li>Oil and Gas Investments</li>
                        <li>Judgments/Structured Settlements</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
