@extends('app')

@section('content')

    <div id="aboutus">
        <div class="container">
            <div class="section_header">
                <h3>Tax Liens/Tax Deeds</h3>
            </div>
            <div class="row">
                <div class="col-sm-6 intro">
                    <p>
                        UniCredit Capital Investors are often attracted to tax liens/tax deeds because of the relatively low capital necessary, the potential returns and the ability to be involved in real estate without much of the responsibility of owning the actual property.
                    </p>
                    <p>
                        A tax lien is a lien imposed on property by law to secure payment of taxes. Tax liens may be imposed for delinquent taxes owned on real property or personal property, as well as a result of failure to pay income taxes or other taxes. It is imposed by the county in which the property is located.
                    </p>
                </div>
                <div class="col-sm-6">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="images/products/tax-liens-1.jpg" alt=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row m-top-50">
                <div class="col-sm-12">
                    <p>
                        UniCredit Capital  will purchase the lien from the county for the possibility of one of two profitable results. First, the tax lien could be redeemed, meaning the owner will pay the lien amount plus interest (a rate that is established by each state) to the investor. If the owner does not pay the lien within a certain time, the investor is given deed to the property.
                    </p>
                    <p>
                        Some states don’t offer tax liens, but rather tax deeds. A tax deed sale is the forced sale conducted by a governmental agency of real estate for nonpayment of taxes. In this case, UniCredit Capital has an opportunity to buy a property deed at discount.
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')

@endsection
