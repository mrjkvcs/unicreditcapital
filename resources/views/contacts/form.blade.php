{!! Form::open() !!}
    <div class="row form">
        <div class="col-sm-6 row-col">
            <div class="box">

                <!-- Name Form Input -->
                <div class="form-group">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                </div>

                <!-- Email Form Input -->
                <div class="form-group">
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                </div>

                <!-- Phone Form Input -->
                <div class="form-group">
                    {!! Form::label('phone', 'Phone:') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
                </div>
                
            </div>
        </div>
        <div class="col-sm-6">

            <!-- Message Form Input -->
            <div class="box">
                {!! Form::label('messages', 'Message:') !!}
                {!! Form::textarea('messages', null, ['class' => 'form-control', 'placeholder' => 'Type a message here...']) !!}
            </div>

        </div>
    </div>

    <div class="row submit">
        <div class="col-md-3 right">
            {!! Form::submit('Send your message', ['class' => 'btn btn-primary']) !!}
        </div>
    </div>

{!! Form::close() !!}
