@extends('app')

@section('content')

    <div id="contact">
        <div class="container">
            <div class="section_header">

                @include('errors.lists')

                <h3>Get in touch</h3>
            </div>
            <div class="row contact">
                <p>
                    UniCredit Capital  stands prepared to assist you and your family in building substantial wealth through our various Real Estate opportunities.
                </p>
                <p>
                    Please contact us via telephone or email to arrange an investment consultation today. Whether you are interested in transforming your underperforming retirement accounts and/or other languishing assets into profitable REO ownership , or if you simply want to explore the proven and time-tested power, safety, and security of UniCredit Capital various opportunities ,   contact us today.
                </p>
                <p>
                    Your American dream awaits you!
                </p>

                @include('contacts.form')

            </div>
        </div>

        <div class="map">
            <div class="container">
                <div class="box_wrapp">
                    <div class="box_cont">
                        <div class="head">
                            <h6>Contact</h6>
                        </div>
                        <ul class="street">
                            <li>Great American Capital Plaza</li>
                            <li>8350 W. Sahara Avenue #190</li>
                            <li>Las Vegas ,NV 89117</li>
                            <li class="icon icontop">
                                <span class="contacticos ico1"></span>
                                <span class="text">1 (800) 315-0585</span>
                            </li>
                            <li class="icon">
                                <span class="contacticos ico2"></span>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.mx/?ie=UTF8&amp;ll=64.089157,-21.816616&amp;spn=0.045157,0.15398&amp;t=m&amp;z=13&amp;output=embed"></iframe>
        </div>
    </div>

@endsection