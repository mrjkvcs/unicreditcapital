<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact</title>
</head>
<body>
    From: {!! $name !!} <br/>
    Email: {!! $email !!} <br/>
    Phone: {!! $phone !!} <br/>
    Message: {!! $messages !!}
</body>
</html>