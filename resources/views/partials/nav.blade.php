<div class="navbar navbar-inverse {!! isset($navbar) ? $navbar : 'navbar-static-top' !!}" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{!! url('/') !!}" class="navbar-brand">
                {{--<strong>UNICREDITCAPITAL</strong>--}}
                <img src="images/uni-logo.png" alt="" style="width: 220px" />
            </a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse" role="navigation">
            <ul class="nav navbar-nav navbar-right">
                <li>{!! link_to_route('home', 'HOME') !!}</li>
                <li>{!! link_to_route('about_path', 'ABOUT US') !!} </li>
                <li>{!! link_to_route('solutions_path', 'SOLUTIONS') !!} </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">PRODUCTS <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>{!! link_to_route('real_estate_path', 'Real Estate') !!}</li>
                        <li>{!! link_to_route('private_lending', 'Private Lending') !!}</li>
                        <li>{!! link_to_route('tax_liens', 'Tax Liens') !!}</li>
                        <li>{!! link_to_route('private_placements', 'Private Placements') !!}</li>
                        <li>{!! link_to_route('eb_5_visa', 'EB-5 Visa ') !!}</li>
                        <li>{!! link_to_route('alternative_investments', 'Other Alternative Investments Allowed in Your IRA') !!}</li>
                    </ul>
                </li>
                <li>{!! link_to_route('contacts_path', 'CONTACT') !!}</li>
                @if ($currentUser)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{!! $currentUser->name !!}<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>{!! link_to_route('logout_path', 'Log out') !!}</li>
                        </ul>
                    </li>
                @else
                    <li>{!! link_to_route('register_path', 'Sign up') !!}</li>
                    <li>{!! link_to_route('login_path', 'Sign in') !!}</li>
                @endif
            </ul>
        </div>
    </div>
</div>
