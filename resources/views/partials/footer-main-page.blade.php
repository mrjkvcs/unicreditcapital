<!-- starts footer -->
<footer id="footer">
    <div class="container">
        <div class="row sections">
            <div class="col-sm-6 testimonials">
                <h3 class="footer_header">
                    Testimonials
                </h3>
                <div class="wrapper">
                    <div class="quote">
                        <span>“</span>
                        <strong>Many thanks to UniCredit Capital !</strong><br/>
                        Thanks to UniCredit Capital agents for providing us the 'Rollover IRA into Real Estate Solution'. We have purchased multiple properties with UniCredit Capital Positive CashFlow LLC program
                        <span></span>
                    </div>
                    <div class="author">
                        <img src="/images/user-display.png" />
                        <div class="name">Frank & Mary</div>
                        <div class="info">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 contact">
                <h3 class="footer_header">
                    Contact
                </h3>

                {!! Form::open(['route' => 'contacts_path']) !!}

                    <!-- Name Form Input -->
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Your name']) !!}

                    <!-- Email Form Input -->
                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Your email']) !!}

                    <!-- Message Form Input -->
                        {!! Form::textarea('messages', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Your message']) !!}

                    {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}

            </div>
        </div>
        <div class="row credits">
            <div class="col-md-12">
                <div class="row social">
                    <div class="col-md-12">
                        <a href="#" class="facebook">
                            <span class="socialicons ico1"></span>
                            <span class="socialicons_h ico1h"></span>
                        </a>
                        <a href="#" class="twitter">
                            <span class="socialicons ico2"></span>
                            <span class="socialicons_h ico2h"></span>
                        </a>
                        <a href="#" class="gplus">
                            <span class="socialicons ico3"></span>
                            <span class="socialicons_h ico3h"></span>
                        </a>
                        <a href="#" class="flickr">
                            <span class="socialicons ico4"></span>
                            <span class="socialicons_h ico4h"></span>
                        </a>
                        <a href="#" class="pinterest">
                            <span class="socialicons ico5"></span>
                            <span class="socialicons_h ico5h"></span>
                        </a>
                        <a href="#" class="dribble">
                            <span class="socialicons ico6"></span>
                            <span class="socialicons_h ico6h"></span>
                        </a>
                        <a href="#" class="behance">
                            <span class="socialicons ico7"></span>
                            <span class="socialicons_h ico7h"></span>
                        </a>
                    </div>
                </div>
                <div class="row copyright">
                    <div class="col-md-12">
                        © Copyright UniCredit 2015 All Rights Reserved
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
