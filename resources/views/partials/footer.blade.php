<footer id="footer">
    <div class="container">
        <div class="row info">
            <div class="col-sm-6 residence">
                <ul>
                    <li>Great American Capital Plaza</li>
                    <li>8350 W. Sahara Avenue #250</li>
                    <li>Las Vegas ,NV 89117</li>
                </ul>
            </div>
            <div class="col-sm-5 touch">
                <ul>
                    <li><strong>P.</strong> 1 (800) 315-0585</li>
                </ul>
            </div>
        </div>
        <div class="row credits">
            <div class="col-md-12">
                <div class="row social">
                    <div class="span12">
                        <a href="#" class="facebook">
                            <span class="socialicons ico1"></span>
                            <span class="socialicons_h ico1h"></span>
                        </a>
                        <a href="#" class="twitter">
                            <span class="socialicons ico2"></span>
                            <span class="socialicons_h ico2h"></span>
                        </a>
                        <a href="#" class="gplus">
                            <span class="socialicons ico3"></span>
                            <span class="socialicons_h ico3h"></span>
                        </a>
                        <a href="#" class="flickr">
                            <span class="socialicons ico4"></span>
                            <span class="socialicons_h ico4h"></span>
                        </a>
                        <a href="#" class="pinterest">
                            <span class="socialicons ico5"></span>
                            <span class="socialicons_h ico5h"></span>
                        </a>
                        <a href="#" class="dribble">
                            <span class="socialicons ico6"></span>
                            <span class="socialicons_h ico6h"></span>
                        </a>
                        <a href="#" class="behance">
                            <span class="socialicons ico7"></span>
                            <span class="socialicons_h ico7h"></span>
                        </a>
                    </div>
                </div>
                <div class="row copyright">
                    <div class="col-md-12">
                        © Copyright UniCredit 2015 All Rights Reserved
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
